# Quantum Computing in Finance - Nimoy 👨‍💻
## Hi 👋  This is a proof of concept repository for Qiskit Finance application , here we go! 🚀

<p align="center">
<img src="https://d9hhrg4mnvzow.cloudfront.net/www.nimoy.io/aed098da-nimoylogo-final-ama_10e80fq000000000000028.png" width="360">
</p>

***
### General Info
> In this repository we will navigate and develop a portfolio optimization proof of concept using Qiskit. For this experiment we will use as data input, the prices of the main cryptocurrencies.
The objective is to be able to find the best combination of cryptocurrencies for an investment portfolio, based on its historical performance.

> Disclaimer: The focus of this repository is to bring readers closer to the different functions that Qiskit Financial Application has, it is in no way about investment advice.

WIP
